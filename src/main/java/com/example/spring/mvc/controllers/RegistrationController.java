package com.example.spring.mvc.controllers;

import com.example.spring.mvc.models.User;
import com.example.spring.mvc.models.UserRegistrationDto;
import com.example.spring.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static com.example.spring.mvc.controllers.RegistrationController.Routes.REGISTRATION_ROOT;
import static com.example.spring.mvc.controllers.RegistrationController.Routes.REGISTRATION_SUCCESS;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserRegistrationDto getUserRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String getUserRegistrationForm() {
        return REGISTRATION_ROOT;
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto dto,
                                      BindingResult bindingResult) {
        User existingUser = userService.findByLogin(dto.getLogin());
        if (existingUser != null) {
            bindingResult.rejectValue("login", "403", "There already is an user with that username");
        }

        if (bindingResult.hasErrors()) {
            return REGISTRATION_ROOT;
        }

        userService.save(dto);
        return REGISTRATION_SUCCESS;
    }


    static class Routes {
        static final String REGISTRATION_ROOT = "registration";
        static final String REGISTRATION_SUCCESS = "redirect:/registration?success";
    }
}
