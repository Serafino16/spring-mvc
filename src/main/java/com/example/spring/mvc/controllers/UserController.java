package com.example.spring.mvc.controllers;

import com.example.spring.mvc.models.User;
import com.example.spring.mvc.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

import static com.example.spring.mvc.controllers.UserController.Routes.*;

@Controller
public class UserController {

    @Autowired
    private UserRepository repository;

    // exemplu separat, nefolosit in mvc
    @GetMapping("/greeting/{name}")
    @ResponseBody
    public String sayHello(@PathVariable(name = "name") String name) {
        return "Hello, " + name;
    }

    @GetMapping
    public String getIndexPage(Model model) {
        /*
        Model e interfata de spring care imi asigura un mecanism de a transmite informatii (date)
        pe structura (key, value) catre template-ul de html, unde este extrasa (informatia) pe baza cheii
         */
        List<User> users = repository.findAll();
        if (!users.isEmpty()) {
            model.addAttribute("users", users);
        }
        return INDEX;
    }

    @GetMapping("/signup")
    public String getSignupPage(User user) {
        return ADD_USER;
    }

    @PostMapping("/add-user")
    public String createNewUser(@Valid User user, BindingResult result) {
        /*
        BindingResult este o interfata din Spring care contine (in situatia in care exista)
        erori de validare pe anumite campuri (vezi anotarile @NotBlank din clasa User

        In situatia in care am erori, nu continui cu salvarea, ci reintorc userul spre pagina initala
        unde in interiorul elementelor span voi popula cu textul erorii, extragand folosind numele proprietatii

        ex: <span th:if="${#fields.hasErrors('password')}" th:errors="*{password}" class="text-danger"></span>
        daca gaseste erori pe campul password, atunci extrag mesajul si il afisez
         */
        if (result.hasErrors()) {
            return ADD_USER;
        }

        // daca nu am gasit erori, incerc sa salvez
        repository.save(user);

        // e un 'bug', daca dau 'redirect:' aici merge, in delete imi trebuie 'redirect:/'
        return REDIRECT;
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        repository.deleteById(id);

        /*
        redirect imi apeleaza programatic endpointul mapat la / din controller
        anotarea @GetMapping ar trebui sa mapeze acea adresa, deci redirectul ma duce pe pagina de index,
        unde am deja un model injectat, care este populat acolo
         */
        return REDIRECT + "/";
    }

    @GetMapping("/edit/{id}")
    public String getEditPage(@PathVariable("id") Long id, Model model) {
        // vreau sa extrag un user din db
//        Optional<User> userOptional = repository.findById(id);
//        User existingUser;
//        if (userOptional.isPresent()) {
//            existingUser = userOptional.get();
//        } else {
//            throw new IllegalArgumentException(String.format("Missing user with id: %s", id));
//        }

        User existingUser = repository.findById(id).<IllegalArgumentException>orElseThrow(() -> {
            throw new IllegalArgumentException(String.format("Missing user with id: %s", id));
        });

        model.addAttribute("user", existingUser);

        return UPDATE_USER;
    }

    @PostMapping("/update/{id}")
    public String updateUserAndReturnToIndex(@PathVariable("id") Long id,
                                             @Valid User user,
                                             BindingResult result) {
        if (result.hasErrors()) {
            return UPDATE_USER;
        }
        // TODO - add passwordEncoder
        repository.save(user);

        return REDIRECT + "/";
    }


    /*
    Am creat o clasa statica cu constantele acestea (rutele) deoarece, daca sunt folosite in mai mult de un loc
    si numele fisierului se schimba, atunci e mai simplu si mai sigur sa updatez intr-un singur loc
     */
    static class Routes {
        static final String INDEX = "index";
        static final String ADD_USER = "add-user";
        static final String UPDATE_USER = "update-user";
        static final String REDIRECT = "redirect:";
    }
}
