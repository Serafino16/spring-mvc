package com.example.spring.mvc.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    @GetMapping("/login")
    public String getLoginPage() {
        LOG.info("Am primit un request de login");
        return Routes.LOGIN;
    }

    static class Routes {
        // numele view-ului pe care vreau sa il randez (render)
        static final String LOGIN = "login";
    }
}
