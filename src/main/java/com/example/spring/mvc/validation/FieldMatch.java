package com.example.spring.mvc.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

// unde anume pot pune anotarea
@Target({
        TYPE,
        ANNOTATION_TYPE
})
// cat timp se pastreaza in memorie obiectele anotate
@Retention(RUNTIME)
// marchez ca toolurile de tip javadoc ar tb sa imi genereze documentatie si pt anotarea asta
@Documented
@Constraint(validatedBy = FieldMatchValidator.class)
public @interface FieldMatch {

    String first();
    String second();
    String message() default "{constraint.field-match}";

    Class <?> [] groups() default {};
    Class <? extends Payload> [] payload() default {};

    @Target({
            TYPE,
            ANNOTATION_TYPE
    })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        FieldMatch[] value();
    }
}
