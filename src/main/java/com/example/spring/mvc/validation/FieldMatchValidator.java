package com.example.spring.mvc.validation;

import com.example.spring.mvc.controllers.MainController;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {

    private static final Logger LOG = LoggerFactory.getLogger(FieldMatchValidator.class);

    private String firstNameField;
    private String secondNameField;

    public void initialize(final FieldMatch annotation) {
        this.firstNameField = annotation.first();
        this.secondNameField = annotation.second();
    }

    @Override
    public boolean isValid(Object receivedObject,
                           ConstraintValidatorContext constraintValidatorContext) {
        try {
            final Object firstObject = BeanUtils.getProperty(receivedObject, firstNameField);
            final Object secondObject = BeanUtils.getProperty(receivedObject, secondNameField);

            // verific fie ca ambele campuri sunt necompletate, fie situatia unde primul este completat
            // si al doilea trebuie sa fie egal cu primul
            return firstObject == null && secondObject == null
                    || firstObject != null && firstObject.equals(secondObject);
        } catch (Exception e) {
            /*
            Placeholderele sunt de forma {}
            pt fiecare placeholder tb pusa dupa virgula o sursa de date
            nu tb apelat .toString() pe sursa de date, loggerul va face asta in background
            daca am de afisat o exceptie, pot sa o pun CA ULTIM PARAMETRU, fara a fi nevoie de placeholder
             */
            LOG.error("Am primit o exceptie la validarea campurilor: {}, {}",
                    firstNameField, secondNameField, e);
            return false;
        }
    }
}
