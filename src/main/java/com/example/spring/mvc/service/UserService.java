package com.example.spring.mvc.service;

import com.example.spring.mvc.models.User;
import com.example.spring.mvc.models.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByLogin(String username);

    User save(UserRegistrationDto dto);
}
