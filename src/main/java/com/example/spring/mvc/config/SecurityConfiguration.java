package com.example.spring.mvc.config;

import com.example.spring.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    // am nevoie sa injectez o instanta de service pt a customiza authentication providerul
    // adica mecanismul care o sa se ocupe 'out-of-the-box' de autentificare
    @Autowired
    private UserService userService;

    // creez un bean de password encoder pt a fi injectat in alte metode si in alte clase
    @Bean
    public BCryptPasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // creez un bean de authentication provider unde de fapt customizez ce user service folosesc si ce
    // password encoder folosesc
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userService);
        authenticationProvider.setPasswordEncoder(getPasswordEncoder());

        return authenticationProvider;
    }

    // suprascriu metoda din WebSecurityConfigurerAdapter pentru a folosi authentication providerul
    // definit de mine
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    // suprascriu metoda din WebSecurityConfigurerAdapter pentru a gestiona traficul de requesturi
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // creez un interceptor de requesturi http pentru a securiza resursele
                // pot sa dau disable la autentificare, alte mecanisme de protectie (anti-atacuri)
                // vor ramane enebled
                .authorizeRequests()
                // face match pe un anumit pattern de request
                // ex: http://localhost/registration -> face match
                // http://localhost/api/registration -> nu face match
                .antMatchers(
                        "/registration**",
                        "/css/**",
                        "/js/**",
                        "/img/**",
                        "/webjars/**"
                        // permitAll -> permite toate requesturile sa treaca fara autentificare
                ).permitAll()
                // any request inglobeaza orice alt request care nu a facut match
                // authenticated specifica nevoia de login (tb sa fii autentificat pt a avea acces
                // la resursele respective
                .anyRequest().authenticated()
            .and()
                // extrage toate proprietatile configurabile pt login
                .formLogin()
                // pagina custom de login
                .loginPage("/login")
                .permitAll()
            .and()
                // inglobeaza toate proprietatile configurabile pt logout
                .logout()
                // face o verificare pe url apelat de logout (si il trimite in spring fwk daca este repsectat pattern)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .invalidateHttpSession(true)
                // se asigura ca userul anterior logat nu ramane in cacheul de spring
                .clearAuthentication(true)
                // adresa unde rutez userul dupa ce am facut un logout cu succes
                .logoutSuccessUrl("/login?logout")
                .permitAll();
    }
}
